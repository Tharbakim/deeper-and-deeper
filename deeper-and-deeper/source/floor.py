import pygame
from .entity import Entity

class Floor(Entity):
    images = []
    def __init__(self, position):
        self.image = self.get_image(__class__, 3, weight=[0.98, 0.01, 0.01])
        super().__init__(self.image, list(map(lambda x:x*32, position)))
        self.image.convert()
        self.position = position
        