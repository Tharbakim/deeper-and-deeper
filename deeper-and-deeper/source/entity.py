import pygame
import random
from .spritesheet import Spritesheet


class Entity(pygame.sprite.Sprite):
    def __init__(self, color, pos, *groups):
        super().__init__(*groups)
        self.image = pygame.Surface((32, 32))
        if isinstance(color, pygame.Surface):
            self.image = color
            self.rect = color.get_rect(topleft=pos)
        else:
            self.image.fill(color),
            self.rect = self.image.get_rect(topleft=pos)
        
    def update_rect(self):
        self.rect = pygame.Rect(list(map(lambda x:x*32, self.position)) + [32, 32])
    
    def get_position(self):
        return {
                "x": self.position[0],
                "y": self.position[1]
               }
        
        
    def get_specific_image(self, class_reference, count, offset, colorkey=None):
        if colorkey == None:
            colorkey = pygame.Color('#FFFFFF')
        if not class_reference.images:
            class_reference.images = Spritesheet(str(class_reference.__name__) + '.png').load_strip(count, colorkey=colorkey)
        return class_reference.images[offset]
        
    def get_image(self, class_reference, count, weight=None, colorkey=None):
        if colorkey == None:
            colorkey = pygame.Color('#FFFFFF')
        if not class_reference.images:
            class_reference.images = Spritesheet(str(class_reference.__name__) + '.png').load_strip(count, colorkey=colorkey)
            return random.choice(class_reference.images)
        else:
            if weight == None:
                return random.choice(class_reference.images)
            else:
                return random.choices(population=class_reference.images, weights=weight, k=1)[0]
                