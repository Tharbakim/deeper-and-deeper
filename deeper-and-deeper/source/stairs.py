import pygame
from .entity import Entity

class Stairs(Entity):
    images = []
    def __init__(self, position):
        self.image = self.get_image(__class__, 1)
        super().__init__(self.image, list(map(lambda x:x*32, position)))
        self.position = position