import pygame
import random
from .level import Level
from .room import Room

class LevelGenerator:
    
    def __init__(self):
        self.difficulty = 0
        
    def generateLevel(self, difficulty):
        self.difficulty = difficulty
        self.max_width = 320 + difficulty*16
        self.max_height= 320 + difficulty*16
        
        #iniialize 2d array to represent the map as all "wall"
        self.map = [[0]*self.max_height for _ in range(self.max_width)]
        self.rooms = [self.mergeShape(self.map, self.generate_shape(), (int(self.max_width/16), int(self.max_height/16)))]
        self.expand_map(self.rooms[0])
        self.fix_map_border()
        self.placePlayer()
        self.placeStairs()
        return Level(self.map)
        
    def place_loot(self, room):
        chance = random.randrange(0, 100)
        if chance > 50:
            cell = random.choice(room.cells)
            if self.map[cell[0]][cell[1]] == 1:
                self.map[cell[0]][cell[1]] = 4
        if len(room.cells) > 30:
            chance = random.randrange(0, 100)
            if chance > 50:
                cell = random.choice(room.cells)
                if self.map[cell[0]][cell[1]] == 1:
                    self.map[cell[0]][cell[1]] = 4 
        
    def placeStairs(self):
        i = 1
        room = self.rooms[-i]
        #Find the last valid room with cells
        while room.cells == None:
            i += 1
            room = self.rooms[-i]
        cell = random.choice(room.cells)
        self.map[cell[0]][cell[1]] = 3
        
    def fix_map_border(self):
        for i in range(0, self.max_width):
            self.map[i][0] = 0
            self.map[i][self.max_height - 1] = 0
        for i in range(0, self.max_height):
            self.map[0][i] = 0
            self.map[self.max_width - 1][i] = 0
        
    def expand_map(self, seed_room):
        if len(self.rooms) > 10:
            if random.randrange(0, 100) < 2*len(self.rooms):
                return
        new_room = self.add_room(seed_room)
        if new_room != None:
            self.expand_map(new_room)
        if random.randrange(0, 100) > 75:
            new_room = self.add_room(seed_room)
            if new_room != None:
                self.expand_map(new_room)
            
            
    
    def generate_hallway(self, room=False):
        #50% change to get a horizontal or vertical hallway
        if room == False:
            room = self.rooms[-1]
            
        if room.cells == []:
            return
        if bool(random.getrandbits(1)):
            direction = "vertical"
            cell_count = int(self.max_height/24)
        else:
            direction = "horizontal"
            cell_count = int(self.max_height/24)
        if direction == "vertical":
            #get horizontal edge cell from room
            min_x = min(room.cells, key = lambda i:i[0])[0]
            max_x = max(room.cells, key = lambda i:i[0])[0]
            edge_cells = [cell for cell in room.cells if cell[0] in [min_x, max_x]]
            start = random.choice(edge_cells)
            if start[0] == min_x:
                next_cell = "left"
            else:
                next_cell = "right"
        else:
            #get vertical edge cell from room
            min_y = min(room.cells, key = lambda i:i[1])[1]
            max_y = max(room.cells, key = lambda i:i[1])[1]
            edge_cells = [cell for cell in room.cells if cell[1] in [min_y, max_y]]
            start = random.choice(edge_cells)
            if start[1] == min_y:
                next_cell = "down"
            else:
                next_cell = "up"
        
        cells = []
        for i in range(0, cell_count):
            if next_cell == "left" and start[0] - i in range(0, self.max_width):
                self.map[start[0] - i][start[1]] = 1
                cells.append((start[0] - i,start[1]))
            elif next_cell == "right" and start[0] + i in range(0, self.max_width):
                self.map[start[0] + i][start[1]] = 1
                cells.append((start[0] + i,start[1]))
            elif next_cell == "up" and start[1] + i in range(0, self.max_height):
                self.map[start[0]][start[1] + i] = 1
                cells.append((start[0],start[1] + i))
            elif next_cell == "down" and start[1] - i in range (0, self.max_height):
                self.map[start[0]][start[1] - i] = 1
                cells.append((start[0],start[1] - i))
        room = Room(cells)
        self.rooms.append(room)
        if room.cells:
            return room.cells[-1]
        else:
            return None
    
    def add_room(self, seed_room):
        start = self.generate_hallway(seed_room)
        if start == None:
            return None
        room = self.mergeShape(self.map, self.generate_shape(), (start[0], start[1]))
        self.place_loot(room)
        self.rooms.append(room)
        return room
    
    
    def mergeShape(self, level_map, shape, start):
        width = len(shape[0])
        height = len(shape)
        x, y = start
        x = x - int(width/2)
        y = y - int(height/2)
        cells = []
        for column in range(x, x+width):
            for row in range(y, y+height):
                if column in range(0, self.max_width) and row in range(0, self.max_height):
                    self.map[column][row] = 1
                    cells.append((column, row))
        return Room(cells)
                
                
    def get_max_width(self):
        return self.max_width
        
    def get_max_height(self):
        return self.max_height
    
    def generate_shape(self):
        x = random.randrange(2, int(self.max_height/24))
        y = random.randrange(2, int(self.max_height/24))
        return [[1]*y]*x
        
    def tryPlacingCharacter(self, x, y):
        if self.map[y][x] == 1:
            self.map[y][x] = 2
            return True
        else:
            return False

    def placePlayer(self):
        x, y = (int(self.max_width/2), int(self.max_height/2))
        steps = 1
        if self.tryPlacingCharacter(x, y):
            return
        else:
            while 1:
                x -= steps
                if self.tryPlacingCharacter(x, y):
                    break
                y -= steps
                if self.tryPlacingCharacter(x, y):
                    break
                steps += 1
                x += steps
                if self.tryPlacingCharacter(x, y):
                    break
                y += steps
                if self.tryPlacingCharacter(x, y):
                    break
                steps += 1
                if steps > (max(self.max_width, self.max_height)):
                    raise Exception('Unable to place character in map')
                        
                
                
            
        
        
        
    
        