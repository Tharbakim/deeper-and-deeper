class Level:
    def __init__(self, world):
        self.world = world
        self.height = len(world)
        self.width = len(world[0])
        
        
    def get_height(self):
        return self.height
    
    def get_width(self):
        return self.width
    
    def update_cell(self, x, y, cell_type):
        self.world[x][y] = cell_type
        
    def check_cell(self, x, y):
        return self.world[x][y]