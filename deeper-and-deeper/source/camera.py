import pygame

class Camera(object):
    def __init__(self, screen_size, world_size):
        self.screen_size = screen_size
        self.world_size = world_size
        self.state = pygame.Rect(0, 0, screen_size.width, screen_size.height)
        
    def apply(self, target):
        return target.rect.move(self.state.topleft)
        
    def update(self, target):
        self.state = self.reposition(target.rect)
        
    def reposition(self, target):
        l, t, _, _ = target # l = left,  t = top
        return pygame.Rect(-l+self.screen_size.width/2-16, -t+self.screen_size.height/2-16, self.screen_size.width, self.screen_size.height)