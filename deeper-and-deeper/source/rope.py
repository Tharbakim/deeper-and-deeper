import random

class Rope:
    
    def __init__(self, length):
        self.length = length
        
    def step(self):
        self.length -= 1
        if self.length <= 0:
            return False
        return True
    
    def get_length(self):
        return self.length
    
    def add_rope(self, length = 0):
        if length == 0:
            chance = random.randrange(0, 100)
            if chance < 20:
                self.length += 5
            elif chance > 85:
                self.length += 50
            else:
                self.length += 15
        else:
            self.length += length
            
