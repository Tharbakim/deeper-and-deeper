import pygame
import pygame_menu
import sys
import random
import os
from .player import Player
from .enemy import Enemy
from .wall import Wall
from .loot import Loot
from .stairs import Stairs
from .camera import Camera
from .rope import Rope
from .levelGenerator import LevelGenerator
from .floor import Floor
from .spritesheet import Spritesheet
class LevelManager:
    floor_tiles = []
    def getSurroundingWalls(self, x, y):
        x1, x2, y1, y2 = 0, 0, 0, 0
        try:
            if self.world.world[y][x + 1] != 0:
                x1 = 1
        except IndexError:
            x1 = 0
        try:
            if self.world.world[y][x - 1] != 0:
                x2 = 1
        except IndexError:
            x2 = 0
        try:
            if self.world.world[y + 1][x] != 0:
                y1 = 1
        except IndexError:
            y1 = 0
        try:
            if self.world.world[y - 1][x] != 0:
                y2 = 1
        except IndexError:
            y2 = 0
        return (x1, x2, y1, y2)
    
    def show_loading(self):
        self.screen.fill((0,0,0))
        loading = self.font.render("Loading", False, (255,255,255))
        self.screen.blit(loading, ((self.screen_size.width - loading.get_width()) / 2, (self.screen_size.height - loading.get_height()) / 2))
        pygame.display.update()

    def generate_world(self):
        pygame.mixer.music.fadeout(1)
        pygame.mixer.music.unload()
        self.show_loading()
        self.world = self.world_generator.generateLevel(self.difficulty)
        self.world_tiles = {
            "x":self.world.get_width(),
            "y":self.world.get_height()
            }
        self.world_size = pygame.Rect(0, 0, self.tile_size.width * self.world_tiles["x"] + self.screen_size.width, self.tile_size.height * self.world_tiles["y"] + self.screen_size.height)
        self.entities = pygame.sprite.Group()
        self.loot = pygame.sprite.Group()
        self.floor = pygame.sprite.Group()
        
        for column in range(0, self.world_tiles["x"]):
            for row in range(0, self.world_tiles["y"]):
                #Proper switch statements are a feature of Python 3.10, this can be improved once 3.10 is available
                
                
                if(self.world.world[column][row] == 0):
                    orientation = self.getSurroundingWalls(row, column)                        
                    self.entities.add(Wall((column, row), orientation))
                    if sum(list(orientation)) != 0:
                        self.floor.add(Floor((column, row)))
                elif(self.world.world[column][row] == 1):
                    #Empty space
                    pass
                elif(self.world.world[column][row] == 2):
                    self.player = Player("Player", (column, row))
                    self.entities.add(self.player)
                    
                elif(self.world.world[column][row] == 4):
                    self.loot.add(Loot((column, row)))
                    
                elif(self.world.world[column][row] == 3):
                    self.entities.add(Stairs((column, row)))
                if self.world.world[column][row] != 0:
                    self.floor.add(Floor((column, row)))
            #self.entities.add(Enemy[("Enemy", (4, 4)))
        self.inputLock = 0
        self.camera = Camera(self.screen_size, self.world_size)
        song = random.randint(1,3)
        (pygame.mixer.music.load(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'sound', 'background' + str(song) + '.mp3')))
        pygame.mixer.music.play(loops=-1)

    def __init__(self, screen, gameClock, FPS):
        self.difficulty = 1
        #Valid states: 0(Paused), 1(Running)
        self.state = 1
        self.score = 50
        self.rope = Rope(50)
        self.screen = screen
        self.bonuses = 0
        self.gameClock = gameClock
        self.FPS = FPS
        self.tile_size = pygame.Rect(0, 0, 32, 32)
        self.screen_size = pygame.Rect(0, 0, 640, 360)
        self.world_generator = LevelGenerator()
        self.font = pygame.font.SysFont('monospace', 28)

        
        self.generate_world()
        



        
    def next_level(self):
        self.difficulty += 1
        self.generate_world()
    
    def setState(self, state):
        self.state = state
        
    def startGame(self):
        sound = pygame.mixer.Sound(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'sound', 'enter.wav'))
        sound.set_volume(0.7)
        sound.play()
        self.difficulty = 1
        self.state = 1
        self.rope = Rope(self.score)
        self.generate_world()
        self.gameLoop()
        
    def resumeGame(self):
        self.state = 1
        self.gameLoop()
        
    def pause(self):
        pause = pygame_menu.Menu('Paused', 400, 250, 
                                theme=pygame_menu.themes.THEME_BLUE)
        pause.add.button('Resume', self.gameLoop, True)
        pause.add.button('Restart (50 Rope)', self.startGame)
        pause.add.button('Quit', pygame_menu.events.EXIT)
        pause.mainloop(self.screen)
    
    def game_over(self):
        self.score = self.difficulty * 50 + self.bonuses
        self.bonuses = 0
        pause = pygame_menu.Menu('Game Over (' + str(self.score) + ')', 400, 250, 
                                theme=pygame_menu.themes.THEME_BLUE)
        pause.add.button('Delve again with ' + str(self.score) + ' Rope', self.startGame)
        pause.add.button('Quit', pygame_menu.events.EXIT)
        pause.mainloop(self.screen)
    
    def handleMovement(self, entity, direction):
        current_position = entity.get_position()
        if direction == "left":
            new_position = {"x": current_position["x"] -1,
                            "y": current_position["y"]}
            self.player.look_left()
        elif direction == "right":
            new_position = {"x": current_position["x"] +1,
                            "y": current_position["y"]}
            self.player.look_right()
        elif direction == "up":
            new_position = {"x": current_position["x"],
                            "y": current_position["y"]-1}
            self.player.look_up()
        elif direction == "down":
            new_position = {"x": current_position["x"],
                            "y": current_position["y"]+1}
            self.player.look_down()
        else:
            raise Exception ("Unknown movement attempted")
            
        destination = self.world.check_cell(new_position["x"], new_position["y"])
        if destination == 1:
            self.rope.step()
            #Cell is empty, go ahead and move
            self.world.update_cell(current_position["x"], current_position["y"], 1)
            self.world.update_cell(new_position["x"], new_position["y"], 2)
            entity.set_position(new_position["x"], new_position["y"])
        elif destination == 4:
            self.rope.step()
            for sprite in self.loot:
                if sprite.position == (new_position["x"], new_position["y"]):
                    self.loot.remove(sprite)
                    
            collision = pygame.sprite.spritecollideany(self.player, self.loot)
            self.loot.remove(collision)
            
            self.rope.add_rope()
            self.world.update_cell(current_position["x"], current_position["y"], 1)
            self.world.update_cell(new_position["x"], new_position["y"], 2)
            entity.set_position(new_position["x"], new_position["y"])

        elif destination == 3:
            #Cell is the stairs, time for a new level
            self.next_level()
            
        
    def handleInput(self):
        
        for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.state = 0
                    pygame.quit()
                    sys.exit()
                    
        keys = pygame.key.get_pressed()
            
        if keys[pygame.K_p] or keys[pygame.K_ESCAPE]:
            self.state = 0
            self.pause()
            return
        if not self.rope.get_length():
            #Out of rope, game over menu
            self.game_over()
            
        else:
            if keys[pygame.K_LEFT] or keys[pygame.K_a]:
                self.handleMovement(self.player, "left")
            
            elif keys[pygame.K_UP] or keys[pygame.K_w]:
                self.handleMovement(self.player, "up")
                
            elif keys[pygame.K_DOWN] or keys[pygame.K_s]:
                self.handleMovement(self.player, "down")
                
            elif keys[pygame.K_RIGHT] or keys[pygame.K_d]:
                self.handleMovement(self.player, "right")
                
    def gameLoop(self, resume=False):
        if resume == True:
            self.state = 1
        while self.state == 1:
            #Capture input
            self.handleInput()
            self.screen.fill((0,0,0))
            self.camera.update(self.player)
            for e in self.floor:
                self.screen.blit(e.image, self.camera.apply(e))
            for e in self.entities:
                self.screen.blit(e.image, self.camera.apply(e))
            #Loot needs to be a separate collection in order to allow collision detection with the player, since the player exists in self.entities
            for e in self.loot:
                self.screen.blit(e.image, self.camera.apply(e))
        #   self.entities.draw(self.screen)
            self.scoreboard = self.font.render("Rope: " + str(self.rope.get_length()), False, (255, 255, 255))
            self.screen.blit(self.scoreboard, (0, 0))
            self.levelboard = self.font.render("Level: " + str(self.difficulty), False, (255,255,255))
            self.screen.blit(self.levelboard, (self.screen_size.width - self.levelboard.get_width(), 0))
            
            pygame.display.update()
            self.gameClock.tick(self.FPS)
                
