import pygame
from .entity import Entity


class Wall(Entity):
    images = []
    def __init__(self, position, orientation):
        count = sum(list(orientation))
                     # Orientation is in format (down, up, right, left)
        if count == 0:
            # No surrounding empty spaces
            self.image=pygame.Surface((32, 32))
            self.image.fill(pygame.Color("#000000"))
            self.image.convert()
        elif count == 1:
            # Wall is part of a wall
            self.image = self.get_specific_image(__class__, 5, 0, colorkey=pygame.Color("#FFFFFF"))
            if orientation[0] == 1:
                self.image = pygame.transform.rotate(self.image, 90)
            if orientation[1] == 1:
                self.image = pygame.transform.rotate(self.image,270)
            if orientation[2] == 1:
                self.image =pygame.transform.rotate(self.image, 180)
                #Left wall?!

        elif count == 2:
            # Wall is wither a corner or empty on 2 opposite sides

            if orientation[0] == 1 and orientation[1] == 1:
                self.image = self.get_specific_image(__class__, 5, 1, colorkey=pygame.Color("#FFFFFF"))
                self.image = pygame.transform.rotate(self.image, 90)
                # Vertical narrow wall
            elif orientation[2] == 1 and orientation[3] == 1:
                # Horizontal narrow wall
                self.image = self.get_specific_image(__class__, 5, 1, colorkey=pygame.Color("#FFFFFF"))

            else:
                self.image = self.get_specific_image(__class__, 5, 2, colorkey=pygame.Color("#FFFFFF"))

                if orientation[0] == 1 and orientation[2] == 1:
                    self.image = pygame.transform.rotate(self.image, 90)

                elif orientation[0] == 1 and orientation[3] == 1:
                    self.image = pygame.transform.rotate(self.image, 0)

                elif orientation[1] == 1 and orientation[2] == 1:
                    self.image = pygame.transform.rotate(self.image, 180)
                
                elif orientation[1] == 1 and orientation[3] == 1:
                    self.image = pygame.transform.rotate(self.image, 270)
                    

        elif count == 3:
            self.image = self.get_specific_image(__class__, 5, 3, colorkey=pygame.Color("#FFFFFF"))

            if orientation[1] == 0:
                    self.image = pygame.transform.rotate(self.image, 90)

            elif orientation[0] == 0:
                    self.image = pygame.transform.rotate(self.image, 270)

            elif orientation[3] == 0:
                    self.image = pygame.transform.rotate(self.image, 180)

            # Wall is only connected on one side
        elif count == 4:
            # Single "Island" wall
            self.image = self.get_specific_image(__class__, 5, 4, colorkey=pygame.Color("#FFFFFF"))

        super().__init__(self.image, list(map(lambda x: x*32, position)))
        self.position=position
