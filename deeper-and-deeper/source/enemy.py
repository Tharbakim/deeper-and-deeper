import pygame
from .entity import Entity

class Enemy(Entity):
    def __init__(self, name, position):
        super().__init__(pygame.Color("#FF0000"), list(map(lambda x:x*32, position)))
        self.name = name
        self.image = pygame.Surface((32, 32))
        self.image.fill(pygame.Color("#FF0000"))
        self.image.convert()
        self.position = list(position)
        
    def set_position(self, x, y):
        self.position[0] = x
        self.position[1] = y
        self.update_rect()
        
    def moveLeft(self):
        self.position[0] -= 1
        self.update_rect()
        
    def moveRight(self):
        self.position[0] += 1
        self.update_rect()
        
    def moveUp(self):
        self.position[1] += 1
        self.update_rect()
        
    def moveDown(self):
        self.position[1] -= 1
        self.update_rect()