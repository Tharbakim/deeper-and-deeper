import pygame
from .entity import Entity

class Player(Entity):
    images = []
    def __init__(self, name, position):
        self.image = self.get_image(__class__, 1)
        self.baseimage = self.get_image(__class__, 1)
        super().__init__(self.image, list(map(lambda x:x*32, position)))
        self.name = name
        self.position = list(position)
        
    def set_position(self, x, y):
        self.position[0] = x
        self.position[1] = y
        self.update_rect()
        
    def look_left(self):
        self.image = pygame.transform.rotate(self.baseimage, 90)
            
    def look_right(self):
        self.image = pygame.transform.rotate(self.baseimage, 270)
            
    def look_up(self):
        self.image = self.baseimage
        
    def look_down(self):
        self.image = pygame.transform.rotate(self.baseimage, 180)