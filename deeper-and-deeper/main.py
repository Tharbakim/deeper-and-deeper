import pygame
import pygame_menu
import os
from source.levelManager import LevelManager

#Initialize all the pygame systems. Screw loading only the ones we want, load them all for now
pygame.init()

FPS = 30
gameClock = pygame.time.Clock()
pygame.display.set_caption('Tomb Diver')
pygame.display.set_icon(pygame.image.load(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'img', 'Stairs.png')))

screen=pygame.display.set_mode([640,360])

level_manager = LevelManager(screen, gameClock, FPS)

def main_menu():
    #Define main menu
    menu = pygame_menu.Menu('Tomb Diver', 400, 250, 
                            theme=pygame_menu.themes.THEME_BLUE)
    menu.add.text_input('Name: ', default='Indiana')
    menu.add.button('Start New (50 Rope)', level_manager.startGame)
    menu.add.button('Quit', pygame_menu.events.EXIT)
    menu.mainloop(screen)

def main():
        running = 1
        main_menu()

if __name__ == "__main__":
    main()