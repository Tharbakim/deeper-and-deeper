# Tomb Diver

Tomb Diver is an entry into the Ludum Dare 48 Compo event (Deeper and deeper).

Source code is hosted on [Gitlab](https://gitlab.com/Tharbakim/deeper-and-deeper)

It's a very simple take on a classic dungeon crawler game, however it is void of traditional "enemies". Your score is derived from how many floors you can traverse before you run out of steps (rope). Upon returning to the menu, you are given 50 units of rope for each floor you managed to clear in addition to your starting 50. In theory, this allows you to delve "deeper and deeper" with each run.

There are 3 different sprites for "loot" items that can generate - a box, a barrel, and a coil of rope. All 3 of these items give you additional rope for your current run.

## Technology
Tomb Diver is written in `python 3.9`, using the `pygame` and `pygame-menu` libraries.

"Art" is done with [Aseprite](https://www.aseprite.org/)

Background sounds created using [beepbox.co](https://www.beepbox.co)

Menu sounds created using [sfxr](https://www.drpetter.se/project_sfxr.html)


## Controls
Standard `wasd` or array keys for movement. `p` or `ESC` for the pause menu. Enter to confirm menu choices.

## Known Bugs

Occasionally, the map generator can generate detached rooms, which subsequently can then contain the stairs within them. I've only seen this twice and both times it has happened on the first room. I believe this bug still exists in the final verison.